package pubnub;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.io.InputStream;
import java.util.Arrays;
import org.apache.cordova.PluginResult;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.pubnub.api.enums.PNPushType;
import com.pubnub.api.models.consumer.push.PNPushAddChannelResult;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.objects_api.channel.PNChannelMetadataResult;
import com.pubnub.api.models.consumer.objects_api.membership.PNMembershipResult;
import com.pubnub.api.models.consumer.objects_api.uuid.PNUUIDMetadataResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.pubnub.api.models.consumer.pubsub.PNSignalResult;
import com.pubnub.api.models.consumer.pubsub.files.PNFileEventResult;
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult;
import com.pubnub.api.models.consumer.history.PNMessageCountResult;
import com.pubnub.api.models.consumer.history.PNFetchMessagesResult;
import com.pubnub.api.models.consumer.history.PNFetchMessageItem;
import com.pubnub.api.models.consumer.message_actions.PNMessageAction;
import com.pubnub.api.models.consumer.message_actions.PNAddMessageActionResult;
import com.pubnub.api.enums.PNPushEnvironment;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class echoes a string called from JavaScript.
 */
public class PubNubWrapper extends CordovaPlugin {
    CallbackContext callbackContext;
    PubNub pubnub;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        // creating random string for UUid
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(7);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }

        // setting pn configuration
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(args.getString(1));
        pnConfiguration.setPublishKey(args.getString(2));
        pnConfiguration.setUuid(randomStringBuilder.toString());

        if (pubnub == null) {
            pubnub = new PubNub(pnConfiguration);
        }

        // checking action

        if (action.equals("publish")) {
            this.publish(args.getString(0), args.getString(3), pubnub, callbackContext);
            return true;
        } else if (action.equals("presenceEvent")) {
            this.presenceEvent(args.getString(0), pubnub);
            return true;
        } else if (action.equals("signalEvent")) {
            this.signalEvent(args.getString(0), args.getString(3), pubnub, callbackContext);
            return true;
        } else if (action.equals("messageCountEvent")) {
            this.messageCountEvent(args.getString(0), pubnub, callbackContext);
            return true;
        } else if (action.equals("fetchMessagesEvent")) {
            this.fetchMessagesEvent(args.getString(0), pubnub, callbackContext);
            return true;
        } else if (action.equals("addMessageActionEvent")) {
            this.addMessageActionEvent(args.getString(0), args.getString(3), args.getString(4), pubnub,
                    callbackContext);
            return true;
        } else if (action.equals("addFcmNotificationEvent")) {
            this.addFcmNotificationEvent(args.getString(0), args.getString(3), pubnub, callbackContext);
            return true;
        } else if (action.equals("addApnsNotificationEvent")) {
            this.addApnsNotificationEvent(args.getString(0), args.getString(3), args.getString(4), args.getString(5),
                    pubnub, callbackContext);
            return true;
        } else if (action.equals("unSubscribe")) {
            this.unSubscribeChannels(args.getString(0), pubnub, callbackContext);
            return true;
        } else if (action.equals("main")) {
            this.main(args.getString(0), pubnub, callbackContext);
            return true;
        }
        return false;
    }

    private void signalEvent(String channels, String message, PubNub pubnub, CallbackContext callbackContext) {
        pubnub.signal().message(message).channel(Arrays.asList(channels.trim().split(",")).get(0))
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult pnPublishResult, PNStatus pnStatus) {
                        if (pnStatus.isError()) {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("signal", pnPublishResult.getTimetoken());
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        } else {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("signal",
                                    "pnStatus.getErrorData().getThrowable().printStackTrace().toString()");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void presenceEvent(String channels, PubNub pubnub) {
        pubnub.subscribe().channels(Arrays.asList(channels.trim().split(","))) // subscribe to channels
                .withPresence() // also subscribe to related presence information
                .execute();
    }

    private void messageCountEvent(String channels, PubNub pubnub, CallbackContext callbackContext) {
        Long lastHourTimetoken = (Calendar.getInstance().getTimeInMillis() - TimeUnit.HOURS.toMillis(1)) * 10000L;
        pubnub.messageCounts().channels(Arrays.asList(channels.trim().split(",")))
                .channelsTimetoken(Arrays.asList(lastHourTimetoken)).async(new PNCallback<PNMessageCountResult>() {
                    @Override
                    public void onResponse(PNMessageCountResult result, PNStatus status) {
                        JsonObject messageCountObject = new JsonObject();
                        if (!status.isError()) {
                            for (Map.Entry<String, Long> messageCountEntry : result.getChannels().entrySet()) {
                                messageCountObject.addProperty("messageCountChannel", messageCountEntry.getKey());
                                messageCountObject.addProperty("messageCountNumber", messageCountEntry.getValue());
                            }
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    messageCountObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        } else {
                            messageCountObject.addProperty("messageCountChannel", "failed");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    messageCountObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void fetchMessagesEvent(String channels, PubNub pubnub, CallbackContext callbackContext) {
        Long lastHourTimetoken = (Calendar.getInstance().getTimeInMillis() - TimeUnit.HOURS.toMillis(1)) * 10000L;
        pubnub.fetchMessages().channels(Arrays.asList(channels.trim().split(","))).end(lastHourTimetoken)
                .maximumPerChannel(100).async(new PNCallback<PNFetchMessagesResult>() {
                    @Override
                    public void onResponse(PNFetchMessagesResult result, PNStatus status) {
                        JsonObject messageCountObject = new JsonObject();
                        if (!status.isError()) {
                            Map<String, List<PNFetchMessageItem>> channels = result.getChannels();
                            messageCountObject.addProperty("fetchMessages", channels.toString());
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    messageCountObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        } else {
                            messageCountObject.addProperty("fetchMessages", "failed");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    messageCountObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void addMessageActionEvent(String channels, String setType, String setValue, PubNub pubnub,
            CallbackContext callbackContext) {
        Long lastHourTimetoken = (Calendar.getInstance().getTimeInMillis() - TimeUnit.HOURS.toMillis(1)) * 10000L;
        pubnub.addMessageAction().channel(Arrays.asList(channels.trim().split(",")).get(0)).messageAction(
                new PNMessageAction().setType(setType).setValue(setValue).setMessageTimetoken(lastHourTimetoken))
                .async(new PNCallback<PNAddMessageActionResult>() {
                    @Override
                    public void onResponse(PNAddMessageActionResult result, PNStatus status) {
                        JsonObject messageActionObject = new JsonObject();
                        if (!status.isError()) {
                            messageActionObject.addProperty("messageActionType", result.getType());
                            messageActionObject.addProperty("messageActionValue", result.getValue());
                            messageActionObject.addProperty("messageActionUuid", result.getUuid());
                            messageActionObject.addProperty("messageActionTimetoken", result.getActionTimetoken());
                            messageActionObject.addProperty("messageActionMessageTimetoken",
                                    result.getMessageTimetoken());
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    messageActionObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        } else {
                            messageActionObject.addProperty("messageAction", "failed");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    messageActionObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void addFcmNotificationEvent(String channels, String deviceToken, PubNub pubnub,
            CallbackContext callbackContext) {
        pubnub.addPushNotificationsOnChannels().pushType(PNPushType.FCM)
                .channels(Arrays.asList(channels.trim().split(","))).deviceId(deviceToken)
                .async(new PNCallback<PNPushAddChannelResult>() {
                    @Override
                    public void onResponse(PNPushAddChannelResult result, PNStatus status) {
                        if (!status.isError()) {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("fcmNotification", status.toString());
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);

                        } else {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("fcmNotification", "failed");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void addApnsNotificationEvent(String channels, String deviceToken, String topic, String environment,
            PubNub pubnub, CallbackContext callbackContext) {
        pubnub.addPushNotificationsOnChannels().pushType(PNPushType.APNS2)
                .channels(Arrays.asList(channels.trim().split(","))).deviceId(deviceToken).topic(topic)
                .environment(
                        environment.equals("PRODUCTION") ? PNPushEnvironment.PRODUCTION : PNPushEnvironment.DEVELOPMENT)
                .async(new PNCallback<PNPushAddChannelResult>() {
                    @Override
                    public void onResponse(PNPushAddChannelResult result, PNStatus status) {
                        if (!status.isError()) {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("apnsNotification", status.toString());
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);

                        } else {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("apnsNotification", "failed");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void publish(String channels, String message, PubNub pubnub, CallbackContext callbackContext) {
        JsonObject entryUpdate = new JsonObject();
        entryUpdate.addProperty("message", message);

        pubnub.publish().channel(Arrays.asList(channels.trim().split(",")).get(0)).message(entryUpdate)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        if (!status.isError()) {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("messsageStatus", "sent");
                            statusJsonObject.addProperty("timetoken", result.getTimetoken());
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);

                        } else {
                            JsonObject statusJsonObject = new JsonObject();
                            statusJsonObject.addProperty("status", "failed");
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                                    statusJsonObject.toString());
                            pluginResult.setKeepCallback(true);
                            callbackContext.sendPluginResult(pluginResult);
                        }
                    }
                });
    }

    private void main(String channels, PubNub pubnub, CallbackContext callbackContext) {
        pubnub.addListener(subscribeCallback);
        pubnub.subscribe().channels(Arrays.asList(channels.trim().split(","))).execute();
    }

    private void unSubscribeChannels(String channels, PubNub pubnub, CallbackContext callbackContext) {

        pubnub.unsubscribe().channels(Arrays.asList(channels.trim().split(","))).execute();
        JsonObject presenceJsonObject = new JsonObject();
        presenceJsonObject.addProperty("unsubcribe_status", "leave");
        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, presenceJsonObject.toString());
        pluginResult.setKeepCallback(true);
        callbackContext.sendPluginResult(pluginResult);

    }

    SubscribeCallback subscribeCallback = new SubscribeCallback() {
        @Override
        public void status(PubNub pubnub, PNStatus status) {

            if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                JsonObject messageJsonObject = new JsonObject();
                messageJsonObject.addProperty("status", "PNUnexpectedDisconnect");
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
            }

            else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
                JsonObject messageJsonObject = new JsonObject();
                messageJsonObject.addProperty("status", "PNConnected");
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);

            } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {
                JsonObject messageJsonObject = new JsonObject();
                messageJsonObject.addProperty("status", "PNReconnected");
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
            } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {
                JsonObject messageJsonObject = new JsonObject();
                messageJsonObject.addProperty("status", "PNDecryptionError");
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
            }
        }

        @Override
        public void message(PubNub pubnub, PNMessageResult message) {

            JsonObject object = message.getMessage().getAsJsonObject();
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("message", object.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

        // Subscribe to realtime Presence events, such as join, leave, and timeout, by
        // UUID
        @Override
        public void presence(PubNub pubnub, PNPresenceEventResult presence) {
            if (presence.getEvent().equals("leave")) {
                JsonObject presenceJsonObject = new JsonObject();
                presenceJsonObject.addProperty("unsubcribe_status", "leave");
                presenceJsonObject.addProperty("unsubcribe_uuid", presence.getUuid());
                presenceJsonObject.addProperty("unsubcribe_occupancy", presence.getOccupancy());
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, presenceJsonObject.toString());
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
            } else {
                JsonObject messageJsonObject = new JsonObject();
                messageJsonObject.addProperty("presence", presence.getEvent());
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
                pluginResult.setKeepCallback(true);
                callbackContext.sendPluginResult(pluginResult);
            }
        }

        @Override
        public void signal(PubNub pubNub, PNSignalResult pnSignalResult) {
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("signal", pnSignalResult.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

        @Override
        public void uuid(PubNub pubNub, PNUUIDMetadataResult pnuuidMetadataResult) {
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("uuid", pnuuidMetadataResult.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

        @Override
        public void channel(PubNub pubNub, PNChannelMetadataResult pnChannelMetadataResult) {
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("channel", pnChannelMetadataResult.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

        @Override
        public void membership(PubNub pubNub, PNMembershipResult pnMembershipResult) {
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("membership", pnMembershipResult.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);

        }

        @Override
        public void messageAction(PubNub pubNub, PNMessageActionResult pnMessageActionResult) {
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("action", pnMessageActionResult.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

        @Override
        public void file(PubNub pubNub, PNFileEventResult pnFileEventResult) {
            JsonObject messageJsonObject = new JsonObject();
            messageJsonObject.addProperty("file", pnFileEventResult.toString());
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, messageJsonObject.toString());
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

    };
}
