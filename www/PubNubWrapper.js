var exec = require("cordova/exec");
exports.publish = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "publish", arg0);
};

exports.presenceEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "presenceEvent", arg0);
};

exports.signalEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "signalEvent", arg0);
};

exports.messageCountEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "messageCountEvent", arg0);
};

exports.fetchMessagesEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "fetchMessagesEvent", arg0);
};

exports.addMessageActionEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "addMessageActionEvent", arg0);
};

exports.addFcmNotificationEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "addFcmNotificationEvent", arg0);
};

exports.addApnsNotificationEvent = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "addApnsNotificationEvent", arg0);
};

exports.main = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "main", arg0);
};

exports.unSubscribe = function (arg0, success, error) {
  exec(success, error, "PubNubWrapper", "unSubscribe", arg0);
};
